import http from "http";
import createError from "http-errors";
import express from "express";
import path from "path";
import cookieParser from "cookie-parser";
import logger from "morgan";

// Initialize express
const app = express();

// view engine setup
// app.set("views", path.join(process.cwd(), "views"));
// app.set("view engine", "jade");
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(process.cwd(), "public")));


// Start the HTTP server on port 3000
app.listen(3000, () => {
  console.log("Server is listening on port 3000");
});
