"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const http_1 = __importDefault(require("http"));
const ws_1 = require("ws");
const events_1 = __importDefault(require("events"));
class Emitter extends events_1.default {
    constructor() {
        super();
    }
    send(message) {
        this.emit('message', message);
    }
}
const emitter = new Emitter();
const app = (0, express_1.default)();
const server = http_1.default.createServer(app);
const wss = new ws_1.WebSocketServer({ noServer: true });
app.get('/', (req, res) => {
    emitter.send('Hello World!');
    res.send('Hello World!');
});
wss.on('connection', (ws) => {
    emitter.on('message', (message) => {
        ws.send(message);
    });
    console.log('WebSocket connected');
    ws.on('message', (message) => {
        console.log('Received:', message);
    });
    ws.send('Welcome to the WebSocket server!');
});
server.on('upgrade', (request, socket, head) => {
    wss.handleUpgrade(request, socket, head, (ws) => {
        wss.emit('connection', ws, request);
    });
});
server.listen(3000, () => {
    console.log('Server is listening on http://localhost:3000');
});
// import http from "http";
// import createError from "http-errors";
// import express from "express";
// import path from "path";
// import cookieParser from "cookie-parser";
// import logger from "morgan";
// import { WebSocketServer } from "ws";
// import { createClient } from "redis";
// import createUser from "./utils/user";
// // Initialize express
// const app = express();
// // Create an HTTP server and attach express to it
// const server = http.createServer(app);
// // Set up WebSocket server
// const wss = new WebSocketServer({ noServer: true });
// server.on("upgrade", (request, socket, head) => {
//   wss.handleUpgrade(request, socket, head, (ws) => {
//     console.log("WebSocket server connected");
//     process.nextTick(() => {
//       wss.emit("connection", ws, request);
//     });
//   });
// });
// // Set up Redis clients
// const redisClient = createClient({ url: "redis://localhost:6379" });
// const redisSubscriberClient = createClient({ url: "redis://localhost:6379" });
// redisClient.on("error", (err) => console.log("Redis Client Error", err));
// redisClient.connect();
// redisSubscriberClient.on("error", (err) =>
//   console.log("Redis Client Error", err)
// );
// redisSubscriberClient.connect();
// // view engine setup
// app.set("views", path.join(process.cwd(), "views"));
// app.set("view engine", "jade");
// app.use(logger("dev"));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());
// app.use(express.static(path.join(process.cwd(), "public")));
// app.get("/redis", async (req, res, next) => {
//   wss.on("connection", (ws) => {
//     console.log("Connected to Redis");
//     ws.on("error", console.error);
//     ws.on("message", (data) => {
//       console.log("received: %s", data);
//     });
//     ws.send("something");
//   });
//   const mockApiCall = () =>
//     new Promise((resolve) => {
//       setTimeout(() => {
//         resolve(createUser());
//       }, 3000);
//     });
//   const cachedData = await redisClient.GET("user");
//   if (cachedData) {
//     return res.json(JSON.parse(cachedData));
//   }
//   const data = await mockApiCall();
//   await redisClient.SETEX("user", 60, JSON.stringify(data));
//   res.json(data);
// });
// // catch 404 and forward to error handler
// app.use((req, res, next) => {
//   next(createError(404));
// });
// // Start the HTTP server on port 3000
// server.listen(3000, () => {
//   console.log("Server is listening on port 3000");
// });
