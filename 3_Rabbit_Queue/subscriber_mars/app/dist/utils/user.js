"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const faker_1 = require("@faker-js/faker");
// Create a function to generate a user object
function createUser() {
    const user = {
        firstName: faker_1.faker.name.firstName(),
        lastName: faker_1.faker.name.lastName(),
        username: faker_1.faker.internet.userName(),
        email: faker_1.faker.internet.email(),
        password: faker_1.faker.internet.password(),
        avatar: faker_1.faker.image.avatar(),
        dateOfBirth: faker_1.faker.date.between("1970-01-01", "2003-12-31"),
        address: {
            street: faker_1.faker.address.streetName(),
            city: faker_1.faker.address.city(),
            state: faker_1.faker.address.state(),
            zipCode: faker_1.faker.address.zipCode(),
            country: faker_1.faker.address.country(),
        },
    };
    return user;
}
exports.default = createUser;
