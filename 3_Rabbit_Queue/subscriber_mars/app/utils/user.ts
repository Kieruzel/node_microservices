import { faker } from "@faker-js/faker";

// Create a function to generate a user object
export default function createUser() {
  const user = {
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    username: faker.internet.userName(),
    email: faker.internet.email(),
    password: faker.internet.password(),
    avatar: faker.image.avatar(),
    dateOfBirth: faker.date.between("1970-01-01", "2003-12-31"),
    address: {
      street: faker.address.streetName(),
      city: faker.address.city(),
      state: faker.address.state(),
      zipCode: faker.address.zipCode(),
      country: faker.address.country(),
    },
  };
  return user;
}
