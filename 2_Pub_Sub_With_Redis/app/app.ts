import express from 'express';
import http from 'http';
import { WebSocketServer } from 'ws';

const app = express();
const server = http.createServer(app);
const wss = new WebSocketServer({ noServer: true });

wss.on('connection', (ws) => {

  console.log('WebSocket connected');
  ws.on('message', (message) => {
    console.log('Received:', message);
  });
  ws.send('Welcome to the WebSocket server!');
});

server.on('upgrade', (request, socket, head) => {
  wss.handleUpgrade(request, socket, head, (ws) => {
    wss.emit('connection', ws, request);
  });
});

server.listen(3000, () => {
  console.log('Server is listening on http://localhost:3000');
});

