"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const amqplib_1 = require("amqplib");
const config_1 = __importDefault(require("../config"));
const consumer_1 = __importDefault(require("./consumer"));
const procuder_1 = __importDefault(require("./procuder"));
class QueueClient {
    constructor() {
        this.isInitialized = false;
    }
    static getInstance() {
        if (!QueueClient.instance) {
            QueueClient.instance = new QueueClient();
        }
        return QueueClient.instance;
    }
    initialize() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.isInitialized) {
                return;
            }
            try {
                this.connection = yield (0, amqplib_1.connect)(config_1.default.rabbitmq.url);
                this.producerChannel = yield this.connection.createChannel();
                this.consumerChannel = yield this.connection.createChannel();
                const q = yield this.consumerChannel.assertQueue(config_1.default.rabbitmq.queue, {
                    // durable: false,
                    exclusive: false,
                });
                console.log("q.queue", q.queue);
                this.consumer = new consumer_1.default(this.consumerChannel, q.queue);
                this.producer = new procuder_1.default(this.producerChannel);
                this.isInitialized = true;
                this.consumer.consumeMessages();
            }
            catch (error) {
                console.error(error);
            }
        });
    }
    sendMessage(msg, corelationId, replayTo) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.isInitialized) {
                yield this.initialize();
            }
            return yield this.producer.produceMessage(msg, corelationId, replayTo);
        });
    }
}
exports.default = QueueClient.getInstance();
