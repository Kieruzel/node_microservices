"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const messageHandler_1 = require("./messageHandler");
class Consumer {
    constructor(channel, rpcQueue) {
        this.channel = channel;
        this.rpcQueue = rpcQueue;
    }
    consumeMessages() {
        return __awaiter(this, void 0, void 0, function* () {
            this.channel.consume(this.rpcQueue, (msg) => __awaiter(this, void 0, void 0, function* () {
                console.log("Msg", msg === null || msg === void 0 ? void 0 : msg.content.toString());
                if ((msg === null || msg === void 0 ? void 0 : msg.properties.replyTo) && (msg === null || msg === void 0 ? void 0 : msg.properties.correlationId)) {
                    const { replyTo, correlationId } = msg === null || msg === void 0 ? void 0 : msg.properties;
                    yield messageHandler_1.MessageHandler.handle(msg === null || msg === void 0 ? void 0 : msg.content.toString(), correlationId, replyTo);
                }
                else {
                    console.log("No replyTo or correlationId");
                }
            }));
        });
    }
}
exports.default = Consumer;
