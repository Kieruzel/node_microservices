"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const client_1 = __importDefault(require("../rabbitmq/client"));
const router = express_1.default.Router();
/* GET home page. */
router.get("/", function (req, res, next) {
    client_1.default.sendMessage("Hello World!");
    res.render("index", { title: "Express" });
});
exports.default = router;
