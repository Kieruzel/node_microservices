export default {
  rpc: {
    host: "localhost",
    port: 8545,
  },
  rabbitmq: {
    url: "amqp://user:pass@localhost",
    queue: "rpc_queue",
  },
};
