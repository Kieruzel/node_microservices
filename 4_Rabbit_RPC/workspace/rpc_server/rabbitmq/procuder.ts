import { Channel } from "amqplib";
import config from "../config";
import { randomUUID } from "crypto";

export default class Producer {
  constructor(private channel: Channel ) {}

  async produceMessage(msg: string, correlationId: string, replayToQueue: string) {
    this.channel.sendToQueue(replayToQueue, Buffer.from(msg), {
        correlationId: correlationId,
        replyTo: replayToQueue,
    });
  }
}
