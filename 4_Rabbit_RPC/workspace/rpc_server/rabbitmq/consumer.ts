import { Channel, ConsumeMessage } from "amqplib";
import { MessageHandler } from "./messageHandler";

export default class Consumer {
  constructor(private channel: Channel, private rpcQueue: string) {}

  async consumeMessages() {
    this.channel.consume(this.rpcQueue, async (msg: ConsumeMessage | null) => {
      console.log("Msg", msg?.content.toString());
      if (msg?.properties.replyTo && msg?.properties.correlationId) {
        const { replyTo, correlationId } = msg?.properties;

        // TODO: Consume messages from the queue
        // TODOL: Handle response

        await MessageHandler.handle(msg?.content.toString(), correlationId, replyTo);
      } else {
        console.log("No replyTo or correlationId");
      }
    });
  }
}
