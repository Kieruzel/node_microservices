import { Channel, Connection, connect } from "amqplib";
import config from "../config";
import Consumer from "./consumer";
import Producer from "./procuder";


class QueueClient {
  private constructor() {}
  private static instance: QueueClient;
  private isInitialized = false;

  static getInstance() {
    if (!QueueClient.instance) {
      QueueClient.instance = new QueueClient();
    }
    return QueueClient.instance;
  }

  private consumer!: Consumer;
  private connection!: Connection;
  private producer!: Producer;
  private producerChannel!: Channel;
  private consumerChannel!: Channel;

  async initialize() {
    if (this.isInitialized) {
      return;
    }

    try {
      this.connection = await connect(config.rabbitmq.url);

      this.producerChannel = await this.connection.createChannel();
      this.consumerChannel = await this.connection.createChannel();

      const q = await this.consumerChannel.assertQueue(config.rabbitmq.queue, {
        // durable: false,
        exclusive: false,
      });

      console.log("q.queue", q.queue)

      this.consumer = new Consumer(this.consumerChannel, q.queue);
      this.producer = new Producer(this.producerChannel);
      this.isInitialized = true;

      this.consumer.consumeMessages();
    } catch (error) {
      console.error(error);
    }
  }

  async sendMessage(msg: string, corelationId: string, replayTo: string) {
    if (!this.isInitialized) {
      await this.initialize();
    }
    return await this.producer.produceMessage(msg, corelationId, replayTo);
  }
}

export default QueueClient.getInstance();
