"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    rpc: {
        host: "localhost",
        port: 8545,
    },
    rabbitmq: {
        url: "amqp://user:pass@localhost",
        queue: "rpc_queue",
    },
};
