import { Channel, ConsumeMessage } from "amqplib";

export default class Consumer {
  constructor(private channel: Channel, private replayQueueName: string) {}

  async consumeMessages() {
    this.channel.consume(
      this.replayQueueName,
      (msg: ConsumeMessage | null) => {
        // TODO: Consume messages from the queue
        console.log("Msg", msg?.content.toString(), msg);
      }
    );
  }
}
