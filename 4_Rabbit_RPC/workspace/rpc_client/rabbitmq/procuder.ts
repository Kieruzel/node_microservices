import { Channel } from "amqplib";
import config from "../config";
import { randomUUID } from "crypto";

export default class Producer {
  constructor(private channel: Channel, private replayQueueName: string) {}

  async produceMessage(msg: string) {
    this.channel.sendToQueue(config.rabbitmq.queue, Buffer.from(msg), {
        replyTo: this.replayQueueName,
        correlationId: randomUUID(),
    });
  }
}
