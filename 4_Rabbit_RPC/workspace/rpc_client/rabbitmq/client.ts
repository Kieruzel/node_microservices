import { Channel, Connection, connect } from "amqplib";
import config from "../config";
import Consumer from "./consumer";
import Producer from "./procuder";

class QueueClient {
  private constructor() {}
  private static instance: QueueClient;
  private isInitialized = false;

  static getInstance() {
    if (!QueueClient.instance) {
      QueueClient.instance = new QueueClient();
    }
    return QueueClient.instance;
  }

  private consumer!: Consumer;
  private connection!: Connection;
  private producer!: Producer;
  private producerChannel!: Channel;
  private consumerChannel!: Channel;

  async initialize() {
    if (this.isInitialized) {
      return;
    }

    try {
      this.connection = await connect(config.rabbitmq.url);

      this.producerChannel = await this.connection.createChannel();
      this.consumerChannel = await this.connection.createChannel();

      const q = await this.consumerChannel.assertQueue('', {
        // durable: false,
        exclusive: false,
      });

      this.consumer = new Consumer(this.consumerChannel, q.queue);
      this.producer = new Producer(this.producerChannel, q.queue);
      this.isInitialized = true;

      this.consumer.consumeMessages();
    } catch (error) {
      console.error(error);
    }
  }

  async sendMessage(msg: string) {
    if (!this.isInitialized) {
      await this.initialize();
    }
    return await this.producer.produceMessage(msg);
  }
}

export default QueueClient.getInstance();
