import express from "express";
import QueueClient from "../rabbitmq/client";
const router = express.Router();

/* GET home page. */
router.get("/", function (req, res, next) {
  // TODO: Send message to RPC server
  QueueClient.sendMessage("Hello World!");

  res.render("index", { title: "Express" });
});

export default router;
