## Translate with microservice ##

1. We're going to create translating service in microservices architecture
2. Server and client is already prepared
3. Add proper comunication between server and client so whenever client send translation request server will response right away and put the translation message in the queue.
4. Spin up couple of clients to check comunication

For translation you can use https://github.com/extensionsapp/translatte library

TIP: Check TODO: to find out where to start