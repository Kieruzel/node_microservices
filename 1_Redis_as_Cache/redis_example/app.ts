import http from "http";
import createError from "http-errors";
import express from "express";
import path from "path";
import cookieParser from "cookie-parser";
import logger from "morgan";
import { WebSocketServer } from "ws";
import { createClient } from "redis";
import createUser from "./utils/user";

// Initialize express
const app = express();

// view engine setup
app.set("views", path.join(process.cwd(), "views"));
app.set("view engine", "jade");
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(process.cwd(), "public")));

app.get("/user/:id", async (req, res, next) => {

  const mockApiCall = () =>
    new Promise((resolve) => {
      setTimeout(() => {
        resolve(createUser());
      }, 3000);
    });

  const data = await mockApiCall();

  res.json(data);
});

app.get("/users", async (req, res, next) => {

  const mockApiCall = () =>
    new Promise((resolve) => {
      setTimeout(() => {

        const users = [];
        for (let i = 0; i < 100; i++) {
          users.push(createUser());
        }
        resolve(users);
      }, 6000);
    });

  const data = await mockApiCall();

  res.json(data);
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// Start the HTTP server on port 3000
app.listen(3000, () => {
  console.log("Server is listening on port 3000");
});
